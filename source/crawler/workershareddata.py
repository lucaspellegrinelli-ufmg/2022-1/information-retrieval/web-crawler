import os
from threading import Lock
from warcio.warcwriter import WARCWriter

class WorkerSharedData:
    MAX_QUEUE_SIZE = 10000

    def __init__(self, queue, visited, lock=None, write_lock=None, archive_size=1000, archive_count=100):
        if lock is None: lock = Lock()
        if write_lock is None: write_lock = Lock()
        
        self.queue = queue
        self.visited = visited
        self.lock = lock
        self.write_lock = write_lock
        self.archive_size = archive_size
        self.archive_count = archive_count

        if not os.path.exists("archives"):
            os.makedirs("archives")

        self.writer_id = 0
        self.current_writer_size = 0
        self.current_out_file = open(self.get_archive_name(), "wb")
        self.writer = WARCWriter(self.current_out_file, gzip=True)

        self.visited_timespan = {}
        self.alive_threads = {}

    def get_archive_name(self):
        from_count = self.writer_id * self.archive_size
        to_count = (self.writer_id + 1) * self.archive_size - 1
        return f"archives/archive-{from_count}-{to_count}.warc.gz"

    def is_queue_filled(self):
        return len(self.queue) > 0

    def is_running(self):
        return self.writer_id < self.archive_count

    def update_writer(self):
        self.current_writer_size += 1

        if self.current_writer_size >= self.archive_size:
            self.current_writer_size = 0
            self.current_out_file.close()
            self.writer_id += 1
            self.current_out_file = open(self.get_archive_name(), "wb")
            self.writer = WARCWriter(self.current_out_file, gzip=True)

    def save_new_urls(self, urls):
        for url in urls:
            if len(self.queue) < WorkerSharedData.MAX_QUEUE_SIZE and url not in self.visited:
                self.queue.append(url)
                self.visited.add(url)