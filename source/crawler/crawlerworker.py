import re
import time
import json
import requests
from io import BytesIO
from threading import Thread
from statistics import median
from bs4 import BeautifulSoup
from reppy.robots import Robots
from urllib.parse import urlparse
from url_normalize import url_normalize
 
from warcio.statusandheaders import StatusAndHeaders
 
class CrawlerWorker(Thread):
    URL_REGEX_MATCHER = "https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\/?"
    USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
 
    def __init__(self, id, shared_data, max_trials=1, sleep_time=250, timeout=1, verbose=False, custom_verbose=False):
        Thread.__init__(self)
        self.id = id
        self.max_trials = max_trials
        self.shared_data = shared_data
        self.sleep_time = sleep_time
        self.timeout = timeout
        self.verbose = verbose
        self.custom_verbose = custom_verbose

    def run(self):
        while True:
            try:
                self.shared_data.lock.acquire()
 
                # If thread shouldn't be running, break out of the loop
                if not self.shared_data.is_running():
                    self.shared_data.lock.release()
                    break
 
                # If queue is empty, run go to next loop
                if not self.shared_data.is_queue_filled():
                    self.shared_data.lock.release()
                    continue
 
                # Get url of crawl to
                next_url = self.shared_data.queue.pop(0)
                host_url = self.get_host_url(next_url)

                self.shared_data.lock.release()

                # Checking if I can crawl there
                allowed, delay = self.robots_allow_crawl(next_url, host_url)
                if not allowed:
                    continue
 
                self.shared_data.lock.acquire()

                # Get another link if we made a request to that host url too soon
                while not self.is_delay_respected(host_url, delay):
                    self.shared_data.lock.release()
                    time.sleep(delay)
                    self.shared_data.lock.acquire()

                self.shared_data.visited_timespan[host_url] = time.time() 
                self.shared_data.lock.release()
 
                # Tries to get and save the page
                trials = 0
                while trials < self.max_trials:
                    trials += 1
                    if self.run_request_trial(next_url, host_url):
                        break
            except Exception as e:
                if self.custom_verbose:
                    print("[Error]", e, next_url)
 
    def run_request_trial(self, target_url, host_url):
        try:
            # Download page
            resp = self.make_request(target_url)
            req_html = self.preprocess_html(resp.text)

            # Skip page if it is not HTML
            if not self.check_if_page_is_html(resp):
                return True

            if self.custom_verbose:
                self.shared_data.alive_threads[self.id] = time.time()
                times = [ts for t, ts in self.shared_data.alive_threads.items()]
                active_threads = list(filter(lambda ts: time.time() - ts < 5, times))
                arch_count = f"Archive={self.shared_data.writer_id}/{self.shared_data.archive_count}"
                old = f"Oldest={round(time.time() - sorted(times)[0], 2)}s"
                mean = f"Median={round(time.time() - median(times), 2)}s"
                active = f"Active={len(active_threads)}"
                url_str = f"Host={host_url}"
                print(f" > {arch_count} {old} {mean} {active} {url_str}")

            # Create WARC Record
            headers_list = resp.raw.headers.items()
            http_headers = StatusAndHeaders("200 OK", headers_list, protocol="HTTP/1.0")
            payload = BytesIO(bytes(req_html, "utf-8"))
            record = self.shared_data.writer.create_warc_record(target_url, "response", payload=payload, http_headers=http_headers)
            
            # Save WARC record to file
            self.save_warc_record(record)

            # Get links
            urls, soup = self.find_links_from_html(host_url, req_html)

            # Print debug info
            if self.verbose:
                self.print_verbose(target_url, soup)

            # Save responses
            self.shared_data.lock.acquire()
            self.shared_data.save_new_urls(urls)
            self.shared_data.lock.release()
            return True
        except Exception as e:
            if self.custom_verbose:
                print("[Error]", e, target_url)
            return False

    def robots_allow_crawl(self, target_url, host_url):
        try:
            robots = Robots.fetch(
                f"{host_url}robots.txt",
                timeout = self.timeout,
                headers = { "User-Agent": CrawlerWorker.USER_AGENT }
            )
            delay = robots.agent(CrawlerWorker.USER_AGENT).delay
            if delay is None:
                delay = self.sleep_time / 1000
            return robots.allowed(target_url, CrawlerWorker.USER_AGENT), delay
        except:
            return True, self.sleep_time / 1000

    def make_request(self, url):
        return requests.get(
            url = url,
            stream = True,
            timeout = self.timeout,
            headers = {
                "Accept-Encoding": "identity",
                "User-Agent": CrawlerWorker.USER_AGENT
            }
        )

    def is_delay_respected(self, host_url, requested_delay):
        # Calculate time between now and last visit
        if host_url in self.shared_data.visited_timespan:
            visited_diff = time.time() - self.shared_data.visited_timespan[host_url]
        else:
            visited_diff = 999999999

        if requested_delay is None:
            return visited_diff > self.sleep_time
        else:
            return visited_diff > requested_delay

    def preprocess_url(self, url, new_url):
        return url_normalize(url[:-1] + new_url if new_url.startswith("/") else new_url)
 
    def preprocess_html(self, html):
        return bytes(html, "utf-8").decode("utf-8", "ignore")
 
    def get_host_url(self, url):
        return "{uri.scheme}://{uri.netloc}/".format(uri=urlparse(url))
 
    def uri_validator(self, url):
        if "javascript:" in url: return False
        if ".htm" in url: return False
        if ".onion" in url: return False
        if ".pdf" in url: return False
        if "booked" in url: return False
        if "nochi" in url: return False
        if "hotelmix" in url: return False
        if "@" in url: return False
        if self.get_host_url(url).count("-") > 1: return False
 
        try:
            result = urlparse(url)
            match = re.search(CrawlerWorker.URL_REGEX_MATCHER, url)
            return all([result.scheme, result.netloc]) and match
        except:
            return False

    def find_links_from_html(self, host_url, html):
        urls = []
        soup = BeautifulSoup(html, "html.parser")
        for link in soup.find_all("a"):
            link_url = link.get("href")
            if link_url is not None and not link_url.startswith("#"):
                preprocessed = self.preprocess_url(host_url, link_url)
                if self.uri_validator(preprocessed):
                    urls.append(preprocessed)
        return urls, soup

    def save_warc_record(self, record):
        self.shared_data.write_lock.acquire()
        try:
            self.shared_data.writer.write_record(record)
            self.shared_data.update_writer()
        except:
            pass
        self.shared_data.write_lock.release()
 
    def get_n_first_words(self, text, n):
        words = text.replace("\n", " ").replace("\t", " ").split(" ")
        words = [w.strip() for w in words if len(w.strip()) > 0]
        return " ".join(words[:n])

    def check_if_page_is_html(self, response):
        try:
            return "content-type" in response.headers and "text/html" in response.headers["content-type"]
        except:
            return False
 
    def print_verbose(self, url, soup):
        print(json.dumps({
            "URL": url,
            "Title": soup.title.text.strip() if soup.title is not None else "",
            "Text": self.get_n_first_words(soup.body.text.strip(), 20) if soup.body is not None else "",
            "Timestamp": int(time.time())
        }))