import time
import math
import argparse
from url_normalize import url_normalize
from crawler.crawlerworker import CrawlerWorker
from crawler.workershareddata import WorkerSharedData

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--seeds", required=True)
parser.add_argument("-n", "--number", type=int, required=True)
parser.add_argument("-d", "--debug", action="store_true", default=False, required=False)
parser.add_argument("-cv", "--customverbose", action="store_true", default=False, required=False)
parser.add_argument("-w", "--workers", type=int, default=100, required=False)
parser.add_argument("-t", "--time", action="store_true", default=False, required=False)
args = parser.parse_args()

def main():
    if args.time: start = time.time()

    seed_file = open(args.seeds, "r")
    seeds = seed_file.readlines()

    queue = [ url_normalize(seed) for seed in seeds ]
    visited = set(queue)

    if args.number >= 1000:
        file_count = math.ceil(args.number / 1000)
        shared_data = WorkerSharedData(queue=queue, visited=visited, archive_count=file_count)
    else:
        shared_data = WorkerSharedData(queue=queue, visited=visited, archive_count=1, archive_size=args.number)
    
    workers = [CrawlerWorker(id, shared_data, verbose=args.debug, custom_verbose=args.customverbose) for id in range(args.workers)]

    for worker in workers: worker.start()
    for worker in workers: worker.join()

    if args.time: print(f"Time = {time.time() - start}")

if __name__ == "__main__":
    main()